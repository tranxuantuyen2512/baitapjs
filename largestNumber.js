function max(arr) {
  arr = arr.filter(it => !isNaN(it));   //  Dùng filter(lambda) để lọc ra những cái là số, còn lại bỏ
  return Math.max(...arr);   // Math.max(n1, n2, n3..., nN) mà đầu vào là array, => số lớn nhất
}

console.log(max([1, 2, 3, 4, 'html']));