function ListNode(data, next = null) {   //triển khai 1 nút danh sách liên kết trong js
    this.data = data;   // giữ dữ liệu của 1 nút
    this.next = next;  // giữ con trỏ đến nút tiếp theo, khởi tạo thành giá trị null
}

function LinkedList(head) {   //triển khai một lớp danh sách liên kết với một phương thức khởi tạo
    this.head = head;   // luu tru nút đầu tiên của danh sách
    // lưu trữ số lượng nút có trong 1 danh sách
}
LinkedList.prototype.append = function (head) {
    let current = this.head;
    while (current.next != null) {
        current = current.next;
    }
    current.next = head;
}

LinkedList.prototype.toString = function () {
    let current = this.head;
    if (current == null)
        return "";
    let res = "" + current.data;

    while (current.next != null) {
        res += " -> " + current.next.data;
        current = current.next;
    }
    return res;
}


// // sắp xếp danh sách
let insertionSort = (head) => {
    let result = null;
    let current = head;
    let next;

    while (current !== null) {
        next = current.next;
        result = sortedInsert(result, current);
        current = next;
    }

    return result;
}
let sortedInsert = (sorted, newNode) => {
    //Temporary node to swap the elements 
    let temp = new ListNode(); // temp = node mới 
    let current = temp;
    temp.next = sorted;

    // sắp xếp danh sách
    while (current.next !== null && current.next.data < newNode.data) {
        current = current.next;
    }

    //Hoán đổi elements
    newNode.next = current.next;
    current.next = newNode;

    return temp.next;
}

let firstNode = new ListNode(4); //tạo 1 node mới
let ll = new LinkedList(firstNode);  // tạo 1 dslk và head = first Node
ll.append(new ListNode(2));
ll.append(new ListNode(6));
ll.append(new ListNode(3));

console.log(ll.toString()); //trc khi sắp xếp
let headSorted = insertionSort(ll.head);

let llSorted = new LinkedList(headSorted);
console.log(llSorted.toString()); //sau khi sắp xếp


// reference:
// https://www.geeksforgeeks.org/implementation-linkedlist-javascript/
// https://learnersbucket.com/examples/algorithms/sorting-a-linked-list/
// https://www.youtube.com/watch?v=9YddVVsdG5A