function findLongestWord(str) {
    let strSplit = str.split(' '); //tách chuỗi thành mảng các chuỗi nhỏ
    let longestWord;  
    let longestLen = 0; //tạo biến giữ độ dài của từ dài nhất
    for (let i = 0; i < strSplit.length; i++) {  //dùng vòng lặp for, cho nó chạy các chuỗi nhỏ trong mảng strSplit.length
        if (strSplit[i].length > longestLen) {
            longestWord = strSplit[i];
            longestLen = strSplit.length;
        };
    }
    return longestWord; // Return the longest word
}
let result = findLongestWord("the world map");
console.log(result);  //(world)
