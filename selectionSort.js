function selectionSort(array) {
    for (let i = 0; i < array.length - 1; i++) {
        // tìm số nhỏ nhất trong mảng con
      let idmin = i;
      for (let j = i + 1; j < array.length; j++) {
        if (array[j] < array[idmin]){
            idmin = j;
        } 
      }
  
      // swappinng hoán đổi các phần tử
    if (idmin != i)  {
        let t = array[i];
      array[i] = array[idmin];
      array[idmin] = t;
    }
      
    }
    return array;
  }    
let str = selectionSort([9, 1, 6, 12]);
console.log(str);

