function insertionSort(inputArr) {    // thuật toán insertionSor
    let n = inputArr.length;
    for (let i = 1; i < n; i++) {
        // lấy phần tử đầu tiên trong mảng con khi chưa đc sắp xếp
        let current = inputArr[i];
        // phần tử cuối của mảng con đc sắp xếp
        let j = i - 1;
        while ((j > -1) && (current < inputArr[j])) {
            inputArr[j + 1] = inputArr[j];
            j--;
        }
        inputArr[j + 1] = current;
    }
    return inputArr;
}
let t = insertionSort([21, 2, 94, 88]);
console.log(t); //[2, 21, 88, 94]
