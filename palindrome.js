// string doi xung
function palindrome(str) {
    let re = /[^A-Za-z0-9]/g;   // khong nam trong khoang nay
    let lowRegStr = str.toLowerCase().replace(re, ''); // đưa chuỗi về chữ thường và loại bỏ các kí tự không nằm trong khoảng này
    let reverseStr = lowRegStr.split('').reverse().join(''); //tách chuỗi thành mảng, đảo mảng và gộp mảng lại thành chuỗi
    return reverseStr === lowRegStr; //2 chuỗi giống nhau => true
}
let result = palindrome("abcdcgfg")
console.log(result);
