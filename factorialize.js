function factorial(num) {   // factorialize a number with recursion (đệ quy)
    if (num < 0)
        return -1;
    else if (num == 0)
        return 1;
    else {
        return (num * factorial(num - 1));  // gọi chính hàm đó trong nó
    }
}
let result = factorial(5)
console.log(result);

