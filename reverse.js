// case 1
function ReverseString(str) {
    return str.split('').reverse().join('') //split tách chuỗi str và trả về mảng các chuỗi con
} // đảo ngược các chuỗi con trong mảng và trả về 1 chuỗi

let t = ReverseString("adfghj")
console.log(t);