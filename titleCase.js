function titleCase(str) {
  str = str.toLowerCase().split(' '); // đổi chuỗi thành chữ thường và dùng split tách chuỗi thành mảng các chuỗi nhỏ, thêm dấu cách trong'' để bỏ ký tự cách
  for (let i = 0; i < str.length; i++) { //
    str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1);
  } //charAt để lấy kí tự của chuỗi, (0) là lấy kí tự đầu tiên;
  // toUpperCase() chuyển các phần tử thành chữ in hoa
  //slice trích xuất 1 phần của chuỗi và trả về chuỗi mới, (1) để bỏ kí tự đầu tiên
  return str.join(' '); //chuyển mảng các chuỗi nhỏ thành chuỗi mới
}
let t = titleCase("the coffee house");
console.log(t);

